import express from 'express';
import { connection } from './utils/db.js';
import { Movie } from './models/Movies.js';
import { movieRoutes } from './routes/movies.routes.js';
import { locationRoutes } from './routes/location.routes.js';

// SERVER
const PORT = 3000;
const server = express();

const router = express.Router();

router.get('/', (req, res) => {
res.send('Hello');
});




server.use(express.json());
server.use(express.urlencoded({ extended: true }));
// server.use('/', router);

//rutas
server.use('/movies', movieRoutes);
server.use('/locations', locationRoutes);

//control error

server.use('*', (req, res, next) => {
	const error = new Error('Route not found'); 
	error.status = 404;
	next(error); // Lanzamos la función next() con un error
});

server.use((err, req, res, next) => {
	return res.status(err.status || 500).json(err.message || 'Unexpected error');
});



server.listen(PORT, () => {
console.log(`Server running in http://localhost:${PORT}`);
});
