import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const locationSchema = new Schema(
  {
    name: { type: String, required: true },
    location: { type: String, required: true },
		// Tipo mongoose Id y referencia al modelo Movie
    movies: [{ type: mongoose.Types.ObjectId, ref: 'Movie' }],
  },
  {
    timestamps: true,
  }
);

const Location = mongoose.model('Location', locationSchema);
export { Location }