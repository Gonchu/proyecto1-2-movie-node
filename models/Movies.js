// Archivo Movies.js dentro de la carpeta models
import mongoose from 'mongoose';

const Schema = mongoose.Schema;

// Creamos el esquema de peliculas
const movieSchema = new Schema(
{
    title: { type: String, required: true },
    director: { type: String, required: true },//La propiedad required hace que el campo sea obligatorio
    year: { type: Number },
    genre: { type: String, required: true },
},
{
    // Esta propiedad servirá para guardar las fechas de creación y actualización de los documentos
    timestamps: true,
}
);

// Creamos y exportamos el modelo Movie
const Movie = mongoose.model('Movie', movieSchema);

export { Movie }