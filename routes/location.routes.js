import express from 'express';
import { Location } from '../models/Location.js';
const router = express.Router();


router.get('/', async (req, res, next) => {
    try {
        const locations = await Location.find().populate('movies');
        return res.status(200).json(locations)
    } catch (error) {
        return next(error)
    }
});

router.post('/', async (req, res, next) => {
    try {
        const newLocation = new Location({
            name: req.body.name,
            location: req.body.location,
            movies: []
        });
        const createdLocation = await newLocation.save();
        return res.status(201).json(createdLocation);
    } catch (error) {
        next(error);
    }
});

router.put('/add-movie', async (req, res, next) => {
    try {
        const { locationId } = req.body;
        const { movieId } = req.body;
        const updatedLocation = await Location.findByIdAndUpdate(
            locationId,
            { $push: { movies: movieId } },
            { new: true }
        );
        return res.status(200).json(updatedLocation);
    } catch (error) {
        return next(error);
    }
});

router.delete('/', async (req, res, next) => {
    try {
        const {id} = req.params;
        // No será necesaria asignar el resultado a una variable ya que vamos a eliminarlo
        await Location.findByIdAndDelete(id);
        return res.status(200).json('Movie deleted!');
    } catch (error) {
        return next(error);
    }
});

export { router as locationRoutes }