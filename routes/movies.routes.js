import express from 'express';
import { Movie } from '../models/Movies.js';
const router = express.Router();

router.get('/', (req, res) => {
    return Movie.find()
    .then(movies => {
    return res.status(200).json(movies);// Si encontramos los personajes, los devolveremos al usuario
    })
    .catch(err => {
    return res.status(500).json(err);// Si hay un error, enviaremos por ahora una respuesta de error.
    });
});

router.post('/', async (req, res, next) => {
    try {
      // Crearemos una instancia de character con los datos enviados
    const newMovie = new Movie({
        title: req.body.title,
        year: req.body.year,
        director: req.body.director,
        genre: req.body.genre
    });

      // Guardamos el personaje en la DB
    const createdMovie = await newMovie.save();
    return res.status(201).json(createdMovie);
    } catch (error) {
          // Lanzamos la función next con el error para que lo gestione Express
    next(error);
    }
});


router.get('/:id', async (req, res) => {
	const { id } = req.params;
	try {
		const movie = await Movie.findById(id);
		if (movie) {
			return res.status(200).json(movie);
		} else {
			return res.status(404).json(`No character found by this id: ${id}`);
		}
	} catch (err) {
		return res.status(500).json(err);
	}
});

router.put('/:id', async (req, res, next) => {
    try {
        const { id } = req.params; //Recuperamos el id de la url
        const movie = new Movie(req.body); //instanciamos un nuevo Character con la información del body
        movie._id = id; //añadimos la propiedad _id al personaje creado
        await Movie.findByIdAndUpdate(id , movie);
        return res.status(200).json(movie);//Este personaje que devolvemos es el anterior a su modificación
    } catch (error) {
        return next(error);
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        const {id} = req.params;
        // No será necesaria asignar el resultado a una variable ya que vamos a eliminarlo
        await Movie.findByIdAndDelete(id);
        return res.status(200).json('Movie deleted!');
    } catch (error) {
        return next(error);
    }
});

router.get('/title/:title', async (req, res) => {
	const {title} = req.params;

	try {
		const movieByTitle = await Movie.find({ title: title });
		if(movieByTitle.length > 0){
			return res.status(200).json(movieByTitle);
		} else {
			return res.status(404).json(`No character found by this title: ${title}`);
		}
		
	} catch (err) {
		return res.status(500).json(err);
	}
});

router.get('/genre/:genre', async (req, res) => {
	const {genre} = req.params;

	try {
		const movieByGenre = await Movie.find({ genre: genre });
		if(movieByGenre.length > 0){
			return res.status(200).json(movieByGenre);
		} else {
			return res.status(404).json(`No character found by this title: ${genre}`);
		}
		
	} catch (err) {
		return res.status(500).json(err);
	}
});

router.get('/year/:year', async (req, res) => {
	const {year} = req.params;

	try {
		const movieByYear = await Movie.find({ year: { $gt:year} 
		});
		return res.status(200).json(movieByYear);
	} catch (err) {
		return res.status(500).json(err);
	}
});

export { router as movieRoutes }